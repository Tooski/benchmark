/*
 * TCSS 342 Winter 2013 Assignment 4
 */
package maps;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author Joe
 * @version 3/8/13
 */
public class CompareIntegers implements Comparator<Map.Entry<String, Integer>>
{

  @Override
  public int compare(final Entry<String, Integer> the_entry1,
                     final Entry<String, Integer> the_entry2)
  {
    int i = the_entry2.getValue().compareTo(the_entry1.getValue());
    if (i == 0)
    {
      i = the_entry2.getKey().compareToIgnoreCase(the_entry1.getKey());
    }
    return i;
  }

}
