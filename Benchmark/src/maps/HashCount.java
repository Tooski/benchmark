/*
 * TCSS 342 Winter 2013 Assignment 4
 */
package maps;


import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Selects to use a HashMap.
 * @author Josef Nosov
 * @version 3/8/13
 */
public class HashCount extends AbstractMapCount
{

  /**
   * Sends the list to the parent.
   * @param the_list is the list of strings.
   */
  public HashCount(final List<String> the_list)
  {
    super(the_list);
  }
  
  @Override
  protected Map<String, Integer> createMap()
  {
    return new HashMap<>();
  }



}
