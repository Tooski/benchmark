/*
 * TCSS 342 Winter 2013 Assignment 4
 */
package maps;


import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Selects to use a TreeMap.
 * @author Joe
 * @version 3/8/13
 */
public class TreeCount extends AbstractMapCount
{
  /**
   * Sends the list to the parent.
   * @param the_list is the list of strings.
   */
  public TreeCount(final List<String> the_list)
  {
    super(the_list);
  }


  @Override
  protected Map<String, Integer> createMap()
  {
    return new TreeMap<>();
  }

}
