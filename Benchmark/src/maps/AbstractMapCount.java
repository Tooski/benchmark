/*
 * TCSS 342 Winter 2013 Assignment 4
 */
package maps;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;


/**
 * Adds values to the appropriate tree, sorts using the chosen sorting method.
 * 
 * @author Josef Nosov
 * @version 3/8/13
 * 
 */
public abstract class AbstractMapCount implements MapCount
{

  /**
   * The sort time of the data structure.
   */
  private long my_sort_time;

  /**
   * The result of creating an placing variable into the data structure.
   */
  private long my_result_time;

  /**
   * The map that is used.
   */
  private final Map<String, Integer> my_map;

  /**
   * Creates the Tree/Hashmap.
   * 
   * @param the_list the list of strings.
   */
  public AbstractMapCount(final List<String> the_list)
  {
    final long initial = System.nanoTime();

    my_map = createMap();
    for (String s : the_list)
    {
      if (my_map.containsKey(s))
      {
        my_map.put(s, my_map.get(s) + 1);
      }
      else
      {
        my_map.put(s, 1);
      }
    }
    my_result_time = System.nanoTime() - initial;

  }

  /**
   * Which type of map would you like to use?
   * 
   * @return returns either a new treemap or new hashmap.
   */
  protected abstract Map<String, Integer> createMap();

  @Override
  public long resultTime()
  {
    return my_result_time;
  }

  @Override
  public long sortTime()
  {
    return my_sort_time;
  }

  @Override
  public Set<Entry<String, Integer>> sortTreeSet()
  {
    final long initial = System.nanoTime();
    final Set<Map.Entry<String, Integer>> set =
        new TreeSet<>(new CompareIntegers());
    for (Entry<String, Integer> s : my_map.entrySet())
    {
      set.add(s);
    }

    my_sort_time = System.nanoTime() - initial;
    return set;
  }

  @Override
  public PriorityQueue<Entry<String, Integer>> sortMaxHeap()
  {
    final long initial = System.nanoTime();
    final PriorityQueue<Map.Entry<String, Integer>> queue =
        new PriorityQueue<>(my_map.size(), new CompareIntegers());

    for (Entry<String, Integer> s : my_map.entrySet())
    {
      queue.add(s);
    }
    my_sort_time = System.nanoTime() - initial;
    return queue;
  }

  @Override
  public Map<String, Integer> sortTreeMap()
  {
    final long initial = System.nanoTime();

    final Map<String, Integer> tree =
        new TreeMap<String, Integer>(new Comparator<String>()
        {
          public int compare(final String the_key1, final String the_key2)
          {
            int i = my_map.get(the_key2) - my_map.get(the_key1);
            if (i == 0)
            {
              i = the_key2.compareTo(the_key1);
            }
            return i;
          }
        });
    tree.putAll(my_map);

    my_sort_time = System.nanoTime() - initial;
    return tree;
  }

  @Override
  public List<Entry<String, Integer>> sortLinkedList()
  {
    final long initial = System.nanoTime();
    return listHelper(new LinkedList<>(my_map.entrySet()), initial);
  }

  /**
   * The helper method for both linkedlist and arraylist.
   * 
   * @param the_list the type of entry list.
   * @param the_initial is the initial time for to create, add and sort the list.
   * @return returns the list.
   */
  private List<Entry<String, Integer>> listHelper(final List<Entry<String, Integer>> the_list, 
                                                  final long the_initial)
  {
    Collections.sort(the_list, new CompareIntegers());
    my_sort_time = System.nanoTime() - the_initial;
    return the_list;
  }

  @Override
  public List<Entry<String, Integer>> sortArrayList()
  {
    final long initial = System.nanoTime();
    return listHelper(new ArrayList<>(my_map.entrySet()), initial);
  }
  
  @Override
  public int size()
  {
    return my_map.size();
  }

}
