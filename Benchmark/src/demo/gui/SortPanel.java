/*
 * TCSS 342 Winter 2013 Assignment 4
 */
package demo.gui;

import javax.swing.JComboBox;

import util.MapOption.SortType;

/**
 * The sort panel allows choices for sorting.
 * @author Josef Nosov
 * @version 3/8/13
 */
@SuppressWarnings("serial")
public class SortPanel extends AbstractUtilPanel
{

  /**
   * The tooltip for the sort panel.
   */
  private static final String TOOLTIP = 
      "Sort the map using one of the following data structures.";
  /**
   * The type of sorting choices.
   */
  private SortingOption[] my_sorting = {new SortingOption("TreeSet", SortType.TreeSet),
    new SortingOption("TreeMap", SortType.TreeMap),
    new SortingOption("LinkedList", SortType.LinkedList),
    new SortingOption("ArrayList", SortType.ArrayList),
    new SortingOption("PriorityQueue", SortType.PriorityQueue)};
  /**
   * The Combo Box of array types.
   */
  private JComboBox<String> my_combo;

  /**
   * The sort panel creates a panel for sorting.
   */
  public SortPanel()
  {

    super("Sort with");
    setToolTipText(TOOLTIP);
    my_combo = new JComboBox<>();
    my_combo.setToolTipText(TOOLTIP);

    for (int i = 0; i < my_sorting.length; i++)
    {
      my_combo.addItem(my_sorting[i].my_string_sort);

    }
    add(my_combo);
  }

  /**
   * 
   * @return returns currently selected sort.
   */
  public SortType selectedSortType()
  {
    return my_sorting[my_combo.getSelectedIndex()].my_sort;

  }

  /**
   * The sorting options given to the user.
   * 
   * @author Josef Nosov
   * 
   */
  private class SortingOption
  {
    /**
     * The name of the sort.
     */
    private final String my_string_sort;
    /**
     * The sort type.
     */
    private final SortType my_sort;

    /**
     * The constructor.
     * 
     * @param the_string_sort the name of the sort.
     * @param the_sort the sort type.
     */
    public SortingOption(final String the_string_sort, final SortType the_sort)
    {
      my_string_sort = the_string_sort;
      my_sort = the_sort;
    }

  }
}
