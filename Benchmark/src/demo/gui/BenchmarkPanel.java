/*
 * TCSS 342 Winter 2013 Assignment 4
 */
package demo.gui;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

/**
 * The benchmark test JPanel.
 * @author Josef Nosov
 * @version 3/8/13
 *
 */
@SuppressWarnings("serial")
public class BenchmarkPanel extends AbstractUtilPanel
{

  /**
   * The the label.
   */
  private JLabel my_result_label;

  /**
   * The checkbox possibilities to be timed.
   */
  private final JCheckBox[] my_timed = {new JCheckBox("Time the map?"),
    new JCheckBox("Time the sort?")};

  /**
   * The benchmark test.
   */
  public BenchmarkPanel()
  {
    super("Benchmark");
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    add(benchmarkHelper());
    add(new JSeparator(SwingConstants.HORIZONTAL));
    add(resultHelper());
  }

  /**
   * @return returns a JPanel with the result text.
   */
  private JPanel resultHelper()
  {
    final JPanel s = new JPanel(new FlowLayout(FlowLayout.LEFT));
    s.setToolTipText("The result of the benchmark in both nanoseconds and seconds.");
    s.add(new JLabel("Results: "));
    my_result_label = new JLabel();
    my_result_label.setMaximumSize(my_result_label.getPreferredSize());
    s.add(my_result_label);
    return s;
  }

  /**
   * @return returns a JPanel for the creation of benchmark.
   */
  private JPanel benchmarkHelper()
  {
    final JPanel s = new JPanel();
    s.setToolTipText("Select the benchmark time to be recorded.");
    my_timed[0].setToolTipText("How quickly does this map add values?");
    my_timed[1].setToolTipText("How fast does structure sort the map?");

    for (JCheckBox t : my_timed)
    {
      s.add(t);
      t.setSelected(true);
      addMnemonics(t);
    }
    return s;
  }

  /**
   * Sets the string of the label.
   * @param the_str is the string.
   */
  public void setResultText(final String the_str)
  {
    my_result_label.setText(the_str);
  }

  /**
   * @return returns whether map is timed.
   */
  public boolean isMapTimed()
  {
    return my_timed[0].isSelected();
  }

  /**
   * @return returns whether sort is timed.
   */
  public boolean isSortTimed()
  {
    return my_timed[1].isSelected();
  }

}
