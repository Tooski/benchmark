/*
 * TCSS 342 Winter 2013 Assignment 4
 */
package demo.gui;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * The words list.
 * 
 * @author Josef Nosov
 * @version 3/8/13
 * 
 */
@SuppressWarnings("serial")
public class WordListPanel extends AbstractUtilPanel
{

  /**
   * Tooltip.
   */
  private static final String TOOLTIP = "How many words would you like displayed?";

  /**
   * The amount of rows for the word list.
   */
  private static final int ROWS = 10;

  /**
   * The spinners columns.
   */
  private static final int SPINNER_COLUMNS = 3;
  /**
   * The seperator.
   */
  private static final String SEPERATOR = " : ";

  /**
   * the new line.
   */
  private static final String NEW_LINE = "\n";
  /**
   * The amount of words allowed on the screen.
   */
  private int my_amount_of_words = ROWS;
  /**
   * The text area.
   */
  private JTextArea my_text_area;

  /**
   * The Words list.
   */
  public WordListPanel()
  {
    super("Word List");
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    my_text_area = new JTextArea(ROWS, ROWS);
    my_text_area.setEditable(false);
    final JScrollPane scroll_pane =
        new JScrollPane(my_text_area, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                        JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    add(scroll_pane);
    add(createSpinner());
  }

  /**
   * Creates a spinner.
   * 
   * @return returns the spinner.
   */
  private JPanel createSpinner()
  {
    final JPanel jp = new JPanel();
    jp.add(new JLabel("Amount of Words: "));
    ((JComponent) jp.getComponent(0)).setToolTipText(TOOLTIP);

    final JSpinner spinner = new JSpinner(new SpinnerNumberModel(10, 0, Integer.MAX_VALUE, 1));
    spinner.setToolTipText(TOOLTIP);
    final JFormattedTextField ftf =
        ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
    ftf.setFocusable(true);
    ftf.setEditable(true);
    ftf.setColumns(SPINNER_COLUMNS);
    ftf.setHorizontalAlignment(JFormattedTextField.CENTER);
    spinner.addChangeListener(new ChangeListener()
    {
      @Override
      public void stateChanged(final ChangeEvent the_arg)
      {
        my_amount_of_words = (int) spinner.getValue();
      }

    });

    jp.add(spinner);
    return jp;
  }

  /**
   * Prints the words on the sidebar.
   * 
   * @param the_value the value the map's entries.
   */
  public void printWords(final List<Map.Entry<String, Integer>> the_value)
  {

    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < printSize(the_value.size()); i++)
    {
      sb.append(the_value.get(i).getValue() + SEPERATOR + the_value.get(i).getKey());
      if (i + 1 != printSize(the_value.size()))
      {
        sb.append(NEW_LINE);
      }
    }
    my_text_area.setText(sb.toString());
  }

  /**
   * Prints the words on the sidebar.
   * 
   * @param the_value the value the map's entries.
   */
  public void printWords(final Map<String, Integer> the_value)
  {
    printWords(new LinkedList<>(the_value.entrySet()));
  }

  /**
   * Prints the words on the sidebar.
   * 
   * @param the_value the value the map's entries.
   */
  public void printWords(final Set<Map.Entry<String, Integer>> the_value)
  {
    final Iterator<Entry<String, Integer>> it =
        ((TreeSet<Map.Entry<String, Integer>>) the_value).iterator();
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < printSize(the_value.size()); i++)
    {
      final Entry<String, Integer> s = it.next();
      sb.append(s.getValue() + SEPERATOR + s.getKey());
      if (i + 1 != printSize(the_value.size()))
      {
        sb.append(NEW_LINE);
      }
    }
    my_text_area.setText(sb.toString());
  }

  /**
   * Prints the words on the sidebar.
   * 
   * @param the_value the value the map's entries.
   */
  public void printWords(final PriorityQueue<Map.Entry<String, Integer>> the_value)
  {
    final StringBuilder sb = new StringBuilder();
    final int size = printSize(the_value.size());
    for (int i = 0; i < size; i++)
    {
      
      sb.append(the_value.peek().getValue() + SEPERATOR + the_value.peek().getKey());
      if (i + 1 != size)
      {
        sb.append(NEW_LINE);
      }
      the_value.poll();

    }
    my_text_area.setText(sb.toString());
  }

  /**
   * Prints the appropriate number of words.
   * 
   * @param the_size the requested size.
   * @return the possible size.
   */
  private int printSize(final int the_size)
  {
    int numbers = the_size;
    if (my_amount_of_words < the_size)
    {
      numbers = my_amount_of_words;
    }
    return numbers;

  }

}
