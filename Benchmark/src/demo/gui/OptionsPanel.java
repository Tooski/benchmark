/*
 * TCSS 342 Winter 2013 Assignment 4
 */

package demo.gui;

import demo.WordCount;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

import util.FileInput;
import util.FindResults;

/**
 * The options panel.
 * 
 * @author Josef Nosov
 * @version 3/8/13
 * 
 */
@SuppressWarnings("serial")
public class OptionsPanel extends AbstractUtilPanel
{

  /**
   * Blank space.
   */
  private static final String BLANK_SPACE = " ";

  /**
   * Tooltip for samplesizes.
   */
  private static final String TOOLTIP =
      "How many maps would you like to be created for benchmark testing?";

  /**
   * Spinner's max columns.
   */
  private static final int MAX_COLUMNS = 3;

  /**
   * The progress bar's max progress. 100%.
   */
  private static final int MAX_PROG_BAR = 100;

  /**
   * The gui.
   */
  private WordCount my_gui;
  /**
   * The list of words.
   */
  private List<String> my_list;
  /**
   * The start button.
   */
  private JButton my_start_button;

  /**
   * The sample size.
   */
  private int my_sample_size = 1;

  /**
   * the progress bar.
   */
  private JProgressBar my_progress_bar;

  /**
   * the the result label.
   */
  private JLabel my_result_label;

  /**
   * the total words.
   */
  private JLabel my_total_words;

  /**
   * the unique words.
   */
  private JLabel my_unique_words;

  /**
   * the text file name.
   */
  private JLabel my_text_file_name;

  /**
   * The median label.
   */
  private JLabel my_median_label;

  /**
   * Mean label.
   */
  private JLabel my_mean_label;

  /**
   * The constructor for options.
   * 
   * @param the_gui the gui class.
   */
  public OptionsPanel(final WordCount the_gui)
  {
    super("Options");
    my_list = new ArrayList<>();
    my_gui = the_gui;
    my_start_button = new JButton("Start");
    my_result_label = new JLabel();
    my_median_label = new JLabel();
    my_progress_bar = new JProgressBar(0, MAX_PROG_BAR);
    my_unique_words = new JLabel();
    my_total_words = new JLabel();
    add(startPanel());
  }

  /**
   * The total text.
   * 
   * @param the_value the amount of words.
   */
  private void updateTotalText(final int the_value)
  {
    my_total_words.setText("Total Word Count: " + my_list.size());
  }

  /**
   * Creates a panel that allows user to start.
   * 
   * @return the panel.
   */
  private JPanel startPanel()
  {
    my_text_file_name = new JLabel();
    my_mean_label = new JLabel(BLANK_SPACE);
 
    my_start_button.setEnabled(my_list.size() > 0);
    my_total_words.setText(BLANK_SPACE);
    final JButton open_button = new JButton("Open File...");
    open_button.setToolTipText("Open a *.txt file.");
    open_button.addActionListener(new OpenFileListener());
    addMnemonics(open_button);
    add(open_button);
    final JPanel jp = new JPanel();
    my_start_button.addActionListener(new StartListenter());
    my_start_button.setToolTipText("Start the application!");
    final JLabel jl = new JLabel("Sample Size: ");
    jp.add(jl);
    jl.setToolTipText(TOOLTIP);
    jp.add(createSpinner());
    add(jp);
    my_progress_bar.setValue(0);
    my_progress_bar.setStringPainted(true);
    jp.add(my_start_button);
    addMnemonics(my_start_button);
    final JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

    my_result_label.setToolTipText("The cumulative runtime of map and sorting structures.");
    my_median_label.setToolTipText("The median runtime (outliers removed).");
    my_mean_label.setToolTipText("The mean runtime (outliers removed).");
    my_unique_words.setToolTipText("The amount of non-repeating words.");
    my_total_words.setToolTipText("Total alphanumeric words pulled from the text file.");
    my_text_file_name.setToolTipText("The text file selected.");
    my_progress_bar.setToolTipText("How long until the operation is complete.");

    panel.add(threeColumnPanel(my_text_file_name, my_total_words, my_unique_words));
    panel.add(new JSeparator(SwingConstants.HORIZONTAL));
    panel.add(threeColumnPanel(my_result_label, my_median_label, my_mean_label),
              BorderLayout.SOUTH);
    panel.add(new JSeparator(SwingConstants.HORIZONTAL));
    panel.add(my_progress_bar);
    my_gui.add(panel, BorderLayout.SOUTH);
    
    return jp;
  }

  /**
   * Creates a panel with three rows.
   * 
   * @param the_left the left component.
   * @param the_center the center component.
   * @param the_right the right component.
   * @return returns a new panel.
   */
  private JPanel threeColumnPanel(final JComponent the_left,
                                  final JComponent the_center,
                                  final JComponent the_right)
  {
    final JPanel grid_panel = new JPanel(new GridLayout(1, 0));
    final JPanel left_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    left_panel.add(the_left);
    final JPanel center_panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    center_panel.add(the_center);
    final JPanel right_panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    right_panel.add(the_right);
    grid_panel.add(left_panel);
    grid_panel.add(center_panel);
    grid_panel.add(right_panel);
    return grid_panel;
  }


  /**
   * Creates a JSpinner.
   * 
   * @return returns the JSpinner.
   */
  private JSpinner createSpinner()
  {
    final JSpinner spinner = new JSpinner(new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1));
    spinner.setToolTipText(TOOLTIP);
    final JFormattedTextField ftf =
        ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
    ftf.setFocusable(true);
    ftf.setEditable(true);
    ftf.setColumns(MAX_COLUMNS);
    ftf.setHorizontalAlignment(JFormattedTextField.CENTER);
    spinner.addChangeListener(new ChangeListener()
    {
      @Override
      public void stateChanged(final ChangeEvent the_args)
      {
        my_sample_size = (int) spinner.getValue();
      }
    });
    return spinner;

  }

  /**
   * Upon clicking, creates a new class.
   * 
   * @author Joe
   * 
   */
  private class StartListenter implements ActionListener
  {
    @Override
    public void actionPerformed(final ActionEvent the_args)
    {
      new FindResults(my_gui.getMapOption(), my_gui, new ThreadValues());
    }
  }

  /**
   * Opens and imports file with integers as well as adds them to queue.
   */
  private class OpenFileListener implements ActionListener
  {

    @Override
    public void actionPerformed(final ActionEvent the_event)
    {
      final JFileChooser jfc = new JFileChooser();
      jfc.setAcceptAllFileFilterUsed(false);

      jfc.setFileFilter(new TxtFilter());
      if (jfc.showOpenDialog(my_gui) == JFileChooser.APPROVE_OPTION)
      {
        final String str = jfc.getSelectedFile().toString();
        my_list = new ArrayList<>();
        FileInput.fileToQueue(str, my_list);
        my_start_button.setEnabled(my_list.size() > 0);
        updateTotalText(my_list.size());

        my_unique_words.setText("");
        my_text_file_name.setText(str.substring(str.lastIndexOf('\\') + 1, str.length()));

      }
    }
  }

  
  /**
   * Filters only text files.
   * @author Josef Nosov
   *
   */
  class TxtFilter extends FileFilter
  {

    @Override
    public boolean accept(final File the_file)
    {
      boolean result = false;

      if (the_file.getName().lastIndexOf('.') > 0
          && the_file.getName().lastIndexOf('.') < the_file.getName().length() - 1)
      {
        if ("txt".equals(the_file.getName().substring(
            the_file.getName().lastIndexOf('.') + 1).toLowerCase()))
        {
          result = true;
        }
      }
      if (the_file.isDirectory())
      {
        result = true;
      }
      return result;
    }

    @Override
    public String getDescription()
    {
      return "Text Documents (*.txt)";
    }
  }
  
  /**
   * Updates during thread.
   * 
   * @author Joe
   * 
   */
  public class ThreadValues
  {

    /**
     * Does nothing.
     */
    public ThreadValues()
    {
      // does nothing.
    }

    /**
     * set the progress.
     * 
     * @param the_prog the progress.
     */
    public void setProgress(final int the_prog)
    {
      my_progress_bar.setValue(the_prog);
    }

    /**
     * sets the runtime text.
     * 
     * @param the_total_runtime runtime in nanoseconds.
     * @param the_tr_s runtime in seconds.
     * @param the_median_runtime mean runtime in nanoseconds.
     * @param the_mean_runtime runtime in seconds.
     * @param the_medr_s the median runtime.
     * @param the_mr_s the median runtime in seconds.
     */
    public void setRuntimeResults(final long the_total_runtime, final String the_tr_s,
                                  final long the_mean_runtime,  final String the_mr_s, 
                                  final long the_median_runtime, final String the_medr_s)
    {
      setRuntimeResults(the_total_runtime, the_tr_s);
      my_mean_label.setText("Mean: " + the_mean_runtime + setSeconds(the_mr_s));
      my_median_label.setText("Median: " + the_median_runtime + setSeconds(the_medr_s));
    }

    /**
     * The sample size.
     * 
     * @return the sample size.
     */
    public int getSampleSize()
    {
      return my_sample_size;
    }

    /**
     * 
     * @return returns a list of string.
     */
    public List<String> getList()
    {
      return new ArrayList<>(my_list);
    }

    /**
     * @param the_value is the unique value that will be set.
     */
    public void setUniqueValue(final int the_value)
    {
      my_unique_words.setText("Unique Words: " + the_value);
    }

    /**
     * Enables/disables the startButton.
     * 
     * @param the_button the value to enable/disable button.
     */
    public void enableButton(final boolean the_button)
    {
      my_start_button.setEnabled(the_button);
    }

    /**
     * Adds seconds to the value.
     * 
     * @param the_seconds the seconds.
     * @return returns seconds.
     */
    private String setSeconds(final String the_seconds)
    {
      return " (" + the_seconds + "s)";
    }

    /**
     * 
     * @param the_total_runtime the total runtime.
     * @param the_tr_s the total runtime in seconds.
     */
    public void setRuntimeResults(final long the_total_runtime, final String the_tr_s)
    {
      my_result_label.setText("Total: " + the_total_runtime + setSeconds(the_tr_s));
    }
  }

 
}
