/*
 * TCSS 342 Winter 2013 Assignment 4
 */
package util;


/**
 * A node class that allows you to put node objects in.
 * 
 * @author Josef Nosov
 * @version 3/8/13
 * 
 */
public class MapOption
{
  /**
   * Possible sorting options.
   * @author Joe
   *
   */
  public enum SortType
  {
    /**
     * TreeSet sorted by comparator.
     */
    TreeSet, 
    /**
     * TreeMap sorted by comparator.
     */
    TreeMap, 
    /**
     * LinkedList sorted by collections sort.
     */
    LinkedList,
    /**
     * ArrayList sorted by collections sort.
     */
    ArrayList, 
    /**
     * MaxHeap.
     */
    PriorityQueue;
  }

  /**
   * Possibilities for Map types.
   * @author Joe
   *
   */
  public enum MapType
  {
    /**
     * Tree Map is one option.
     */
    TreeMap,
    /**
     * Hashmap is another option.
     */
    HashMap;
  }

  /**
   * They type of structure you will put data into.
   */
  private final MapType my_map_type;
  /**
   * the type of sorting data structure we will use.
   */
  private final SortType my_sort_type;
  /**
   * If we will time the put method.
   */
  private final boolean my_count_put_type;
  /**
   * If we will time the sort?
   */
  private final boolean my_count_sort_type;

  /**
   * The map option node.
   * 
   * @param the_put_type sets the type of map you'd like to put data into.
   * @param the_sort_type sets the type of sorting structure you'd like to use.
   * @param the_count_put_type would you like to time the data structure as it
   *          puts the values in?
   * @param the_count_sort_type would you like to time to data structure as it
   *          sorts the values?
   */
  public MapOption(final MapType the_put_type,
                       final SortType the_sort_type,
                       final boolean the_count_put_type,
                       final boolean the_count_sort_type)
  {
    my_map_type = the_put_type;
    my_sort_type = the_sort_type;
    my_count_put_type = the_count_put_type;
    my_count_sort_type = the_count_sort_type;
  }

  /**
   * @return the my_count_put_type
   */
  public boolean isSortTimed()
  {
    return my_count_put_type;
  }

  /**
   * @return the my_count_sort_type
   */
  public boolean isMapTimed()
  {
    return my_count_sort_type;
  }

  /**
   * @return the my_put_type
   */
  public MapType getMapOption()
  {
    return my_map_type;
  }

  /**
   * @return the my_sort_type
   */
  public SortType getSortOption()
  {
    return my_sort_type;
  }

}
