package util;
/*
 * TCSS 342 Winter 2013 Assignment 4
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

/**
 * Imports file from location on your computer.
 * 
 * @author Josef Nosov
 * @version 3/8/13
 */
public final class FileInput
{
  /**
   * Constructor does nothing.
   */
  private FileInput()
  {
    // Does nothing
  }

  
  
  /**
   * 

   * @param the_location the location of the file.
   * @param the_list the list we would put the text into.
   */
  public static void fileToQueue(final String the_location, final List<String> the_list)
  // O(N)
  {
    Scanner scan = null;
    try
    {
      scan = new Scanner(new File(the_location));
      String value;
      while (scan.hasNext())
      {
        value = scan.next().toLowerCase();
        final StringBuilder sb = new StringBuilder();
        final char[] c = value.toCharArray();
        for (char ch : c)
        {
          if (ch >= 'a' && ch <= 'z' || ch >= '0' && ch <= '9')
          {
            sb.append(ch);
          }
        }
        if (!sb.toString().isEmpty())
        {
          the_list.add(sb.toString());
        }
      }
    }
    catch (final FileNotFoundException e)
    {
      System.err.println("File not found!");
    }
  }

}
