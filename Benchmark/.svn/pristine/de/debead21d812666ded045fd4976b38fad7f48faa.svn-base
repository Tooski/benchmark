/*
 * TCSS 342 Winter 2013 Assignment 4
 */
package util;

import demo.WordCount;
import demo.gui.OptionsPanel.ThreadValues;
import demo.gui.WordListPanel;

import java.awt.Cursor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.SwingWorker;
import maps.HashCount;
import maps.MapCount;
import maps.TreeCount;
import util.MapOption.MapType;
import util.MapOption.SortType;

/**
 * Does all the algorithms in the background to figure out what data structure
 * is required.
 * 
 * @author Josef Nosov
 * @version 3/8/13
 */
public class FindResults
{
  /**
   * Constantly used so created a string representation for this value.
   */
  private static final String RIGHT_BRACE = "s)";

  /**
   * Constantly used so created a string representation for this value.
   */
  private static final String LEFT_BRACE = " (";

  /**
   * Multiply by 100 to get the percent.
   */
  private static final int PERCENT_MODIFIER = 100;

  /**
   * How many nanoseconds per seconds.
   */
  private static final long NANSECONDS_PER_SECONDS = 1000000000;

  /**
   * The IQR range. +- 1.5.
   */
  private static final double IQR_RANGE = 1.5;

  /**
   * The 75% for IQR.
   */
  private static final double PERCENTILE_75 = 0.75;
  /**
   * The 25% for IQR.
   */
  private static final double PERCENTILE_25 = 0.25;

  /**
   * The decimal format.
   */
  private final DecimalFormat my_df = new DecimalFormat("#.00");

  /**
   * The type of map will be used.
   */
  private MapCount my_map;
  /**
   * The map option node.
   */
  private MapOption my_mon;

  /**
   * The gui.
   */
  private WordCount my_gui;

  /**
   * Values that are updated during thread update.
   */
  private ThreadValues my_component;

  /**
   * The default constructor.
   * 
   * @param the_cm the Map option node.
   * @param the_gui the gui.
   * @param the_component is the values updated during thread.
   */
  public FindResults(final MapOption the_cm,
                  final WordCount the_gui, final ThreadValues the_component)
  {
    my_gui = the_gui;
    my_mon = the_cm;
    my_component = the_component;
    final SwingTask t = new SwingTask();
    t.addPropertyChangeListener(new PropertyChangeListener()
    {
      public void propertyChange(final PropertyChangeEvent the_event)

      {
        if ("progress".equals(the_event.getPropertyName()))
        {
          the_component.setProgress((int) the_event.getNewValue());
        }
      }
    });
    the_component.enableButton(false);
    the_gui.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    t.execute();
  }

  /**
   * Uses a hashmap or treemap to create a list.
   * 
   * @param the_put_type the type of value you would like to put in.
   * @param the_list the list of words.
   * @return returns a long.
   */
  private long putType(final MapType the_put_type, final List<String> the_list)
  {
    switch (the_put_type)
    {
      case HashMap:
        my_map = new HashCount(the_list);

        break;
      case TreeMap:

      default:
        my_map = new TreeCount(the_list);

        break;
    }

    return my_map.resultTime();
  }

  /**
   * Sort by this type.
   * 
   * @param the_sort_type the kind of sort you would like to use.
   * @param the_words the words list.
   * @return returns a long for the length of time.
   */
  private long sortType(final SortType the_sort_type, final WordListPanel the_words)
  {
    switch (the_sort_type)
    {
      case TreeSet:
        the_words.printWords((TreeSet<Entry<String, Integer>>) my_map.sortTreeSet());
        break;
      case TreeMap:
        the_words.printWords((TreeMap<String, Integer>) my_map.sortTreeMap());
        break;
      case LinkedList:
        the_words.printWords(my_map.sortLinkedList());
        break;
      case ArrayList:
        the_words.printWords(my_map.sortArrayList());
        break;
      case PriorityQueue:
      default:
        the_words.printWords(my_map.sortMaxHeap());
        break;
    }
    return my_map.sortTime();
  }

  /**
   * 
   * @param the_result_list is the list of results.
   * @return returns the median.
   */
  private long findMedian(final List<Long> the_result_list)
  {
    Collections.sort(the_result_list);
    final long q1 = the_result_list.get((int) ((the_result_list.size() - 1) * PERCENTILE_25));
    final long q3 = the_result_list.get((int) ((the_result_list.size() - 1) * PERCENTILE_75));
    final long irq_less = (long) (q1 - IQR_RANGE * (q3 - q1));
    final long irq_more = (long) (q3 + IQR_RANGE * (q3 - q1));

    final List<Long> list = new ArrayList<>(the_result_list);
    for (long l : list)
    {
      if (l <= irq_less || l >= irq_more)
      {
        the_result_list.remove(l);
      }
    }

    long longnum = 0;
    for (int new_index = 0; new_index < the_result_list.size(); new_index++)
    {
      longnum += the_result_list.get(new_index);
    }
    return longnum / the_result_list.size();
  }

  /**
   * Threaded for progress bar update.
   * 
   * @author Joe
   * 
   */
  class SwingTask extends SwingWorker<Void, Void>
  {

    /**
     * The minimum size before using IQR.
     */
    private static final int MINIMUM_SIZE = 3;

    @Override
    public Void doInBackground()
    {
      setProgress(0);
      final List<Long> long_list = new LinkedList<>();
      long total_runtime = 0;
      for (int i = 0; i < my_component.getSampleSize(); i++)
      {
        final StringBuilder sb = new StringBuilder();

        final long result = putType(my_mon.getMapOption(), my_component.getList());
        final long sort_result = sortType(my_mon.getSortOption(), my_gui.getWordList());
        if (my_mon.isMapTimed() && my_mon.isSortTimed())
        {
          long_list.add(result + sort_result);
          sb.append(my_map.resultTime() + " + " + my_map.sortTime() + " = "
                    + (my_map.resultTime() + my_map.sortTime())
                    + LEFT_BRACE + my_df.format((double) (my_map.resultTime()
                                                + my_map.sortTime()) / NANSECONDS_PER_SECONDS)
                    + RIGHT_BRACE);

        }
        else if (!my_mon.isMapTimed() && my_mon.isSortTimed())
        {
          long_list.add(sort_result);
          sb.append(my_map.sortTime() + LEFT_BRACE + my_df.format((double) my_map.sortTime()
                                                                  / NANSECONDS_PER_SECONDS)
                    + RIGHT_BRACE);
        }

        else if (my_mon.isMapTimed() && !my_mon.isSortTimed())
        {
          long_list.add(result);
          sb.append(my_map.resultTime() + LEFT_BRACE
                    + my_df.format((double) my_map.resultTime()
                                   / NANSECONDS_PER_SECONDS) + RIGHT_BRACE);

        }

        setProgress(Math.min((int) (((double) (i + 1)
                             / my_component.getSampleSize()) * PERCENT_MODIFIER),
                             PERCENT_MODIFIER));
        my_gui.setResultText(sb.toString());
        total_runtime += my_map.resultTime() + my_map.sortTime();
        my_component.setRuntimeResults(total_runtime,
                                       my_df.format((double) total_runtime
                                                    / NANSECONDS_PER_SECONDS));

      }
      my_gui.setCursor(null);
      my_component.enableButton(true);

      long median_value = 0;
      if (my_mon.isMapTimed() || my_mon.isSortTimed())
      {
        if (my_component.getSampleSize() > MINIMUM_SIZE)
        {
          median_value = findMedian(long_list);
        }
        else
        {
          for (int i = 0; i < long_list.size(); i++)
          {
            median_value += long_list.get(i);
          }

          median_value /= long_list.size();

        }
      }
      my_component.setRuntimeResults(total_runtime,
                                     my_df.format((double) total_runtime
                                                  / NANSECONDS_PER_SECONDS),
                                     median_value, my_df.format((double) median_value
                                                                / NANSECONDS_PER_SECONDS));

      my_component.setUniqueValue(my_map.size());
      return null;
    }
  }

}
