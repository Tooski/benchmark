
package demo.gui;

import java.awt.Color;
import java.util.HashSet;
import java.util.Set;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * The abstract utility panel.
 * @author Josef Nosov
 * @version 3/8/13
 */
@SuppressWarnings("serial")
public abstract class AbstractUtilPanel extends JPanel
{
  /**
   * Contains all mnemonics that are currently occupied.
   */
  private static final Set<Character> MNEMONICS_OCCUPIED = new HashSet<Character>();

  /**
   * The parent class.
   * @param the_str the string.
   */
  public AbstractUtilPanel(final String the_str)
  {
    super();
    setBorder(BorderFactory.createTitledBorder(
        BorderFactory.createLineBorder(Color.DARK_GRAY), the_str));
  }

  /**
   * Adds mnemonics if word not occupied.
   * @param the_string the string that will be tested.
   */
  protected void addMnemonics(final AbstractButton the_string)
  {
    final char[] c = the_string.getText().toCharArray();
    for (int i = 0; i < c.length; i++)
    {
      if (!MNEMONICS_OCCUPIED.contains(c[i]))
      {
        MNEMONICS_OCCUPIED.add(c[i]);
        the_string.setMnemonic(c[i]);
        break;
      }
    }
  }

}
