
package maps;

import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * The interface for Mapcount.
 * @author Josef Nosov
 * @version 3/8/13
 */
public interface MapCount
{
  /**
   * @return The result time of putting values into the map.
   */
  long resultTime();

  /**
   * The sorting method for using a tree set.
   * @return returns a sorted set of map entries.
   */
  Set<Map.Entry<String, Integer>> sortTreeSet();

  /**
   * The sorting method for using a priority queue.
   * @return returns a sorted maxheap of map entries.
   */
  PriorityQueue<Map.Entry<String, Integer>> sortMaxHeap();

  /**
   * The sorting method for using a tree map.
   * @return returns a sorted tree map of key/value pairs.
   */
  Map<String, Integer> sortTreeMap();

  /**
   * The sorting method for using a linked list.
   * @return returns a sorted linked list of map entries.
   */
  List<Map.Entry<String, Integer>> sortLinkedList();

  /**
   * The sorting method for using a array list.
   * @return returns a sorted array list of map entries.
   */
  List<Map.Entry<String, Integer>> sortArrayList();
  
  /**
   * @return The sort time of putting values into the chosen data structure.
   */
  long sortTime();

}
