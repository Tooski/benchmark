
package demo.gui;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import util.MapOptionNode.MapType;

/**
 * Allows user to select what kind of map they'd like to use.
 * @author Josef Nosov
 * @version 3/8/13
 *
 */
@SuppressWarnings("serial")
public class StructurePanel extends AbstractUtilPanel
{
  /**
   * The types of structures available.
   */
  private final MapOption[] my_comp = {
    new MapOption("HashMap", MapType.HashMap),
    new MapOption("TreeMap", MapType.TreeMap)};

  /**
   * The data structure panel.
   */
  public StructurePanel()
  {
    super("Data Structure");
    final ButtonGroup g = new ButtonGroup();
    for (int i = 0; i < my_comp.length; i++)
    {
      add(my_comp[i].my_string_sort);
      my_comp[i].my_string_sort.setSelected(true);
      g.add(my_comp[i].my_string_sort);
      super.addMnemonics(my_comp[i].my_string_sort);
    }
  }

  /**
   * @return returns the type of map selected.
   */
  public MapType getMapType()
  {
    MapType maptype;
    if (my_comp[0].my_string_sort.isSelected())
    {
      maptype = my_comp[0].my_map;
    }
    else
    {
      maptype = my_comp[1].my_map;
    }
    return maptype;
  }

  /**
   * The map options.
   * @author Joe
   *
   */
  public class MapOption
  {

    /**
     * The radio button.
     */
    private final JRadioButton my_string_sort;
    /**
     * The amp type.
     */
    private final MapType my_map;

    /**
     * The constructor.
     * 
     * @param the_string_map the name of the sort.
     * @param the_map the sort type.
     */
    public MapOption(final String the_string_map, final MapType the_map)
    {
      my_string_sort = new JRadioButton(the_string_map);
      my_map = the_map;
    }

  }

}
