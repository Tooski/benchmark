/*
 * TCSS 342 Winter 2013 Assignment 4
 */
package demo;

import demo.gui.BenchmarkPanel;
import demo.gui.OptionsPanel;
import demo.gui.SortPanel;
import demo.gui.StructurePanel;
import demo.gui.WordListPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import util.MapOption;

/**
 * The WordCount class, allows use to test hashmap and treemap as well as
 * possible sorting structures and benchmark them.
 * 
 * @author Josef Nosov
 * @version 3/8/13
 * 
 */
@SuppressWarnings("serial")
public class WordCount extends JFrame
{

  /**
   * The panel with list of words.
   */
  private WordListPanel my_words_list;
  /**
   * The benchmark panel.
   */
  private BenchmarkPanel my_benchmark;
  /**
   * The sort panel.
   */
  private SortPanel my_sort;

  /**
   * the structure panel.
   */
  private StructurePanel my_structure;

  /**
   * Starts the GUI.
   */
  public WordCount()
  {
    super("WordCountPlus");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setResizable(false);
    setVisible(true);
    start();
    pack();
    setLocationRelativeTo(null);
  }

  /**
   * sets up the panels.
   */
  private void start()
  {
    initPanels();
    final JPanel left = new JPanel();
    left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
    final JPanel top_panel = new JPanel(new GridLayout(1, 0));
    top_panel.add(my_structure, BorderLayout.WEST);
    top_panel.add(my_sort, BorderLayout.EAST);
    left.add(top_panel);
    left.add(my_benchmark);
    left.add(new OptionsPanel(this));
    add(left, BorderLayout.WEST);
    add(my_words_list, BorderLayout.EAST);
  }

  /**
   * creates the panels.
   */
  private void initPanels()
  {
    my_words_list = new WordListPanel();
    my_benchmark = new BenchmarkPanel();
    my_sort = new SortPanel();
    my_structure = new StructurePanel();
  }

  /**
   * @return Allows specific sorting algorithm to run and update list
   *         appropriately.
   */
  public WordListPanel getWordList()
  {
    return my_words_list;
  }

  /**
   * Creates new MapOptionNode to be able to create map and sort and return
   * appropriate benchmark times.
   * 
   * @return a new map option node when called.
   */
  public MapOption getMapOption()
  {
    return new MapOption(my_structure.getMapType(), my_sort.selectedSortType(),
                             my_benchmark.isMapTimed(), my_benchmark.isSortTimed());
  }

  /**
   * Changes the label of the results.
   * 
   * @param the_str is the string.
   */
  public void setResultText(final String the_str)
  {
    my_benchmark.setResultText(the_str);
  }

  /**
   * The main method.
   * 
   * @param the_args the argument, not used.
   */
  public static void main(final String[] the_args)
  {
    new WordCount();
  }

}
